import argparse
import os
import sys
import traceback
import TestFunctions.Startup as setup
from TestFunctions.Pandell_Test_Page import Test_Page
from TestFunctions.Manager_information import ManagerInformation


"""
TESTCASE_ID: 123
TITLE:      Submit Manager info form with all Manadatory fields entered
OBJECTIVE:  The main purpose of this test case is to add Manager form will all Mandatory
            fields entered
"""

def execute(driver):
    """
    This is function where we add all required steps based on test case steps
    :param driver:
    :return:
    """
    _testpage=Test_Page(driver)
    _testpage.launch_test_page()

    _manager=ManagerInformation(driver)
    _manager.details()

    _manager.address()

    _manager.save()

    _manager.result()


if __name__ == '__main__':
    # This block should be in every script
    parser = argparse.ArgumentParser(description='List of arguments required to run driver script')
    parser.add_argument('-browser', help=['chrome', 'firefox', 'ie'], default='chrome')

    args = parser.parse_args()
    browsername = args.browser

    print("browser is:" + browsername)
    driver=setup.driver_setup(browsername)
    execute(driver)

    setup.teardown(driver)





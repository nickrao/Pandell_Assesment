# Pandell_Assesment

This project is developed to create test cases for Pandell assessment test.

To run this Automation user should install PyCharm IDE and Selenium in his/her machine.

Test Functions Folder: contains all functions required to execute the test case.(eg launching browser, loading test page,
                       and saving form after entering Manager informatiom and Manager address)
Test Cases Folder: This folder contains test cases planned to execute using function from Test functions folder.

After installing PyCharm & Selenium:
-Clone project form git to your local drive
-From your local drive import project to pycharm
-Open a test case under Scripts folder and run it.
-To run on differnet browsers edit configurations and add browser params.By default script will run on Chrome
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.common.by import By
from selenium.webdriver.support.ui import Select
from selenium.common.exceptions import*
from faker import Faker
import random
fake=Faker()


class ManagerInformation:
    def __init__(self,driver):
        """
        Tius function pause the script until selected page is loaded completely
        :param driver:
        """
        self.driver = driver
        self.wait = WebDriverWait(self.driver, 3)
        try:
            self.wait.until(EC.visibility_of_element_located((By.CSS_SELECTOR,"button")))
            print("test  page loaded successfully")
        except:
            raise Exception("unable to load page")

    def autofill_manager_info(self,managerinfo):
        """
        This function will autofill Manager information
        :param managerinfo: First Name, Last Name, Email, Primary phone number
        :return:
        """
        firstName =self.driver.find_element_by_id("firstName")
        firstName.send_keys(managerinfo['firstName'])
        lastName=self.driver.find_element_by_id("lastName")
        lastName.send_keys(managerinfo['lastName'])
        email=self.driver.find_element_by_id("email")
        email.send_keys(managerinfo['email'])
        primary_phone = self.driver.find_element_by_id("phone1")
        primary_phone.send_keys(managerinfo['PrimaryPhone'])
        print("\nManager Information entered as:\nfirstName:%s\nlastName:%s\nemail:%s"
              "\nprimary_phone:%s"%(managerinfo['firstName'],managerinfo['lastName'],
                                    managerinfo['email'],managerinfo['PrimaryPhone']))

    def details(self):
        """
        This function will auto generate Manager information test data
        :return:
        """
        first_name=fake.first_name()
        last_name=fake.last_name()
        email=fake.email()

        # random phone number generator

        area_code = ['403','587','647','780']
        first = random.choice(area_code)
        middle = ''.join(map(str, random.sample(range(10), 3)))
        last = ''.join(map(str, random.sample(range(10), 4)))
        random_phoneNumber = first + '-' + middle + '-' + last
        self.autofill_manager_info({'firstName':first_name,
                                    'lastName': last_name,
                                    'email': email,
                                    'PrimaryPhone':random_phoneNumber})

    def autofill_manager_address(self,address):
        """
        This function will auto fill Manager Address
        :param address:
        :return:
        """
        address1 = self.driver.find_element_by_id("addressLine1")
        address1.send_keys(address['address1'])
        address2 = self.driver.find_element_by_id("addressLine2")
        address2.send_keys(address['address2'])
        city=self.driver.find_element_by_id("city")
        city.send_keys(address['city'])
        province=Select(self.driver.find_element_by_id("province"))
        province.select_by_value(address['province'])
        postal_code = self.driver.find_element_by_id("postalCode")
        postal_code.send_keys(address['postal_code'])
        print("\nManager Address entered as:\naddress1:%s\naddress2:%s"
              "\ncity:%s\nprovince:%s\npostal_code:%s"%(address['address1'],address['address2'],
                                                        address['city'],address['province'],
                                                        address['postal_code']))

    def address(self):
        """
        This function will generate Manager Address test data
        :return:
        """
        address1= fake.street_name()
        address2 =fake.street_suffix()
        city=fake.city()
        # selecting a random province from province dropdown
        province=self.driver.find_element_by_id('province')
        list_provinces=province.find_elements_by_tag_name('option')
        random_province=random.choice(list_provinces).get_attribute('value')
        postal = ['T1T2T3', 'T1T 2T3', 't1t2t3', 't1t 2t3']
        self.autofill_manager_address({'address1': address1,
                                       'address2': address2,
                                       'city': city,
                                       'province': random_province,
                                       'postal_code': random.choice(postal)
                                       })

    def save(self):
        """
        This function is to click on save button to save data
        :return:
        """
        save_button=self.wait.until(EC.element_to_be_clickable((By.CSS_SELECTOR,"button")))
        save_button.click()

    def result(self):
        """
        This function is to validate if Manager info is saved successfully
        :return:
        """
        try:
            self.wait.until(EC.visibility_of_element_located((By.CLASS_NAME,"contact")))
            print("Manager details saved")
        except TimeoutException:
            print("Manager Details not saved")

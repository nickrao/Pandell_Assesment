from selenium import webdriver
import time


def driver_setup(browsername):
    """
    This function will Launch the selected browser from params
    :param browsername: chrome, firefox and IE browsers
    :return:
    """
    if browsername.lower()== 'chrome':
        driver=webdriver.Chrome()
    elif browsername.lower()== 'firefox':
        driver=webdriver.Firefox()
    elif browsername.lower()== 'ie':
        driver=webdriver.Ie()
    return driver


def teardown(driver):
    """
    This function will close the browser and this function is used
    after end of every test case
    :param driver:
    :return:
    """
    time.sleep(5)
    driver.quit()
class Test_Page:
    def __init__(self,driver):
        self.driver = driver

    def launch_test_page(self):
        """
        This function will load the test pase in desired browser
        :return:
        """
        base_url = 'https://qatest.pandell.com/'
        self.driver.get(base_url)  # Sends URL to selenium webdriver browser
